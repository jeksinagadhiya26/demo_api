package com.example.demo_api;

import android.os.AsyncTask;

import com.example.demo_api.DisplayActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class FetchedActivity extends AsyncTask<Void,Void,Void> {

    String data="";
    String singlefetch = "";
    String datafetch = "";
    @Override
    protected Void doInBackground(Void... voids) {

        try {
             URL url = new URL("https://api.jsonbin.io/b/5efdf1000bab551d2b6ab1c9/1");
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            while (line!=null){
                line = bufferedReader.readLine();
                data = data + line;
            }

//            JSONArray ja = new JSONArray(data);
//            for(int i=0;i<ja.length();i++){
//                JSONObject jo = (JSONObject) ja.get(i);
//                singlefetch = "data" + jo.get("data") + "\n" ;
//
//                datafetch = singlefetch + datafetch;
//            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        catch (JSONException e) {
//            e.printStackTrace();
//        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        DisplayActivity.tvdata.setText(this.data);
    }
}
